import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import LoginScreen from './src/screens/Login';
import RegisterScreen from './src/screens/Register';
import HomeScreen from './src/screens/Home';
import CartScreen from './src/screens/Cart';
import DetailtScreen from './src/screens/Details';
import { Provider } from './src/service/provider';
const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Provider>
        <Stack.Navigator screenOptions={{ headerShown: false }}>

          <Stack.Screen name="Login" component={LoginScreen} />
          <Stack.Screen name="Register" component={RegisterScreen} />
          <Stack.Screen name="Home" component={HomeScreen} />
          <Stack.Screen name="Cart" component={CartScreen} options={{ headerShown: true, title: 'Carrinho' }} />
          <Stack.Screen name='Detail' component={DetailtScreen} options={{ headerShown: true, title: 'Detalhe' }} />

        </Stack.Navigator>
      </Provider>
    </NavigationContainer>
  );
}
