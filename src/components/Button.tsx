import React from 'react'
import { TouchableOpacity, StyleSheet, Text, StyleProp } from 'react-native'

const s = StyleSheet.create({
  buttonBox: {
    padding: 10,
    borderRadius: 5,

    width: '80%',

  },
  text: {
    textAlign: 'center',

  },
  primaryButton: {
    backgroundColor: 'rgba(0, 0, 23, 0.5)',
  },
  textPrimary: {
    color: 'white'
  },

})

interface IButton {
  style?: any
  text: string
  onPress?: () => void
  disabled?: boolean
  type?: 'primary' | 'second'
}

const Button = ({ style, text, onPress, disabled = false, type = 'primary' }: IButton) => {
  return (
    <TouchableOpacity style={[s.buttonBox, style, type == 'primary' && s.primaryButton]} onPress={onPress} disabled={disabled}>
      <Text style={[s.text, type == 'primary' && s.textPrimary]}>{text}</Text>
    </TouchableOpacity>
  )
}

export default Button