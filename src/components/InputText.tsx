import React from 'react'
import { TextInput, TextInputProps, StyleSheet } from 'react-native'

const s = StyleSheet.create({ inputDefault: { borderBottomWidth: 1, width: '80%', padding: 10, borderRadius: 5 } })

const InputTextComponent = (props: TextInputProps) => <TextInput {...props} style={[s.inputDefault, props.style]} />

export default InputTextComponent
