import React from 'react'
import { Text, StyleSheet } from 'react-native'


interface ILabelText {
  text: string
  type: 'error' | 'success'
}

const s = StyleSheet.create({
  text: {
    fontSize: 12,
  },
  error: {
    color: 'red'
  },
  success: {
    color: 'green'
  }
})

const LabelText = ({ text, type }: ILabelText) => {
  return (
    <Text style={[
      s.text,
      type === 'error' && s.error,
      type === 'success' && s.success
    ]}>
      {text}
    </Text>
  )
}

export default LabelText