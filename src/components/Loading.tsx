import React from 'react'
import { View, ActivityIndicator, StyleSheet } from 'react-native'

interface ILoading {
  visible: boolean
}

const s = StyleSheet.create({
  box: {
    position: 'absolute', backgroundColor: 'rgba(0, 0, 0, 0.5)', width: '100%', height: '100%', justifyContent: 'center', alignContent: 'center'
  }
})

const Loading = ({ visible }: ILoading) => {
  if (visible)
    return (
      <View style={s.box}>
        <ActivityIndicator />
      </View>
    )
}

export default Loading