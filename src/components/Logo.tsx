import React from 'react'
import { View, StyleSheet } from 'react-native'

const s = StyleSheet.create({
  box: {
    flexDirection: 'row'
  },
  firstBox: {
    width: 50, backgroundColor: 'red', height: 30, borderBottomLeftRadius: 10
  },
  secondBox: {
    width: 50, backgroundColor: 'white', height: 30
  },
  thirdBox: {
    width: 50, backgroundColor: 'black', height: 30, borderTopEndRadius: 10
  }
})

const Logo = () => {
  return (
    <View style={s.box}>
      <View style={s.firstBox} />
      <View style={s.secondBox} />
      <View style={s.thirdBox} />
    </View>
  )
}

export default Logo
