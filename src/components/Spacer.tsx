import React from 'react'
import { View } from 'react-native'

interface ISpacer {
  space?: number
}

const Spacer = ({ space = 1 }: ISpacer) => {
  const spaceTotal = space * 5
  return (
    <View style={{ margin: spaceTotal }} />
  )
}
export default Spacer
