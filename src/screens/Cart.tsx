import React, { useContext, useEffect, useState } from 'react'
import { DataContext } from '../service/provider'
import { View, StyleSheet, Text, SafeAreaView, ScrollView, Image } from 'react-native'
import { IProduct } from '../service/model'
import { useSafeAreaFrame } from 'react-native-safe-area-context'
import { Button, Spacer, Loading, LabelText } from '../components'
import { createOrder } from '../service/api'
const s = StyleSheet.create({
  container: { flex: 1 },
  img: {
    width: 150,
    height: 100,
    marginRight: 20
  },
})

export default function CartScreen() {
  const { width } = useSafeAreaFrame();
  const { cart, products, setCart }: any = useContext(DataContext)
  const [loading, setLoading] = useState(false)
  const [success, setSuccess] = useState(false)

  const calcularTotalCompra = () => {
    return cart.map(item => {
      const find: IProduct = products.find(({ _id }) => item._id === _id)
      return (item.total * parseFloat(find.price)).toFixed(2)
    }).reduce(function (total, numero) {
      return (parseFloat(total) + parseFloat(numero)).toFixed(2);
    }, 0)
  }

  const onConcluirPedido = () => {
    setLoading(true)
    const body = cart.map(item => {
      const find: IProduct = products.find(({ _id }) => item._id === _id)
      return { ...find, quantity: item.total, total: (item.total * parseFloat(find.price)).toFixed(2), description: null, category: null }
    })


    createOrder(body)
      .then(res => { console.log(res) })
      .catch(e => { console.log(e) })
      .finally(() => {
        setLoading(false)
        setCart([])
        setSuccess(true)
      })

  }

  useEffect(() => {
    calcularTotalCompra()
  }, [])

  return (
    <SafeAreaView style={s.container}>
      <Loading visible={loading} />
      <ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1 }} contentContainerStyle={{ flex: 1, alignItems: 'center' }}>

        {cart.length > 0 && cart.map(item => {
          const find: IProduct = products.find(({ _id }) => item._id === _id)
          return (
            < View style={{ backgroundColor: 'white', height: 100, marginBottom: 20, flexDirection: 'row', width: width }} key={find._id}>
              <Image source={{ uri: find.imageUrl }} style={s.img} />
              <View style={{ justifyContent: 'center', alignContent: 'center' }}>
                <Text style={{ fontSize: 18, fontWeight: 'bold', marginBottom: 10 }}>{find.name}</Text>
                <View>
                  <Text style={{ fontSize: 15 }}>Valor</Text>
                  <Text style={{ fontSize: 18, fontWeight: 'bold' }}>R$ {(item.total * parseFloat(find.price)).toFixed(2)}</Text>
                </View>
              </View>
            </View>
          )
        })}


        {cart.length <= 0 && (
          <View style={{ marginVertical: 20 }}><Text>Seu carrinho está vázio :(</Text></View>
        )}

        {success && <LabelText text='Pedido enviado com sucesso!' type='success' />}

        {cart.length > 0 && (
          <>
            <Spacer space={5} />
            <View>
              <Text style={{ fontSize: 15 }}>Valor total do pedido</Text>
              <Spacer />
              <Text style={{ fontSize: 18, fontWeight: 'bold' }}>R$ {calcularTotalCompra()}</Text>
            </View>
            <Spacer space={5} />
            <Button text='Concluir pedido' onPress={onConcluirPedido} />
          </>
        )}

      </ScrollView>
    </SafeAreaView >
  )
}