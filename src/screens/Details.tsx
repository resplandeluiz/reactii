import React from 'react'
import { IProduct } from '../service/model'
import { View, Image, Text, ScrollView } from 'react-native'
import { useSafeAreaFrame } from 'react-native-safe-area-context';
import { Spacer } from '../components';
interface IDetailScreen {
  product: IProduct
}



export default function DetailtScreen(props: any) {
  const { width } = useSafeAreaFrame();
  const { product }: IProduct = props.route.params
  console.log({ product: product._id })
  return (
    <ScrollView style={{ flex: 1 }} contentContainerStyle={{ alignContent: 'center', alignItems: 'center', justifyContent: 'center' }}>
      <Image source={{ uri: product.imageUrl }} style={{ width: width, height: 300 }} />
      <View style={{ flex: 1, marginHorizontal: 20 }}>
        <Spacer />
        <Text style={{ fontSize: 18, fontWeight: 'bold', marginBottom: 20 }}>{product.name}</Text>
        <Spacer />
        <Text>Categoria: {product.category.name}</Text>
        <Text>Unidade : R$ {product.price}</Text>
        <Spacer />
        <Text>{product.description}</Text>

      </View>
      <Spacer space={5} />

    </ScrollView>

  )
}