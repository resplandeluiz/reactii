import React, { useContext, useEffect, useRef, useState } from 'react'
import { getProducts } from '../service/api'
import { SafeAreaView, ScrollView, Text, TouchableOpacity, View, StyleSheet } from 'react-native'
import ProductItem from './partials/ProductItem'
import { useSafeAreaFrame } from 'react-native-safe-area-context'
import { DataContext } from '../service/provider'
import { useNavigation } from '@react-navigation/native'

const s = StyleSheet.create({
  flex1: {
    flex: 1
  },
  padddingTop: {
    paddingTop: 20
  },

  scrollView: {
    flex: 1,
    overflow: 'scroll',
    height: '50%',
    backgroundColor: 'transparent',
    padding: 0,
    margin: 0,
  },
  boxTitulo: {
    flexDirection: 'row', justifyContent: 'space-between', marginBottom: 20, paddingHorizontal: 20, alignContent: 'center', alignItems: 'center'
  },
  titulo: { fontSize: 22, fontWeight: 'bold' },
  boxTotalCarrinho: { backgroundColor: '#3b5998', borderRadius: 50, width: 50, height: 50, alignContent: 'center', justifyContent: 'center' },
  textTotalCarrinho: { color: 'white', fontSize: 20, textAlign: 'center', fontWeight: 'bold' }
})

export default function HomeScreen() {

  const { width } = useSafeAreaFrame();
  const scrollRef = useRef(null);
  const { cart, setCart, setProductsState }: any = useContext(DataContext)
  const [products, setProducts] = useState([])
  const navigation = useNavigation()

  const goToCard = () => navigation.navigate('Cart')

  const totalItemsCarrinho = () => {
    return cart.map(item => item.total).reduce(function (total, numero) {
      return total + numero;
    }, 0)
  }

  const loadProducts = () => {
    getProducts().then(res => {
      setProductsState (res.data)
      setProducts(res.data)
    })
  }

  const updateCart = ({ _id, total }: any) => {
    let items = []
    if (cart?.length) {

      const hasItem = cart.find((item): any => item._id == _id)
      if (hasItem) {
        items = cart.map((item: any) => {
          if (item._id == _id) {
            return { _id, total: total + item.total }
          }
          return item
        })
      } else {
        items = [...cart, { _id, total }]
      }

    } else {
      items = [{ _id, total }]
    }
    setCart(items)
    console.log(items)
  }

  useEffect(() => {
    loadProducts()
  }, [])

  return (
    <SafeAreaView style={s.flex1}>
      <View style={[s.flex1, s.padddingTop]}>

        <View style={s.boxTitulo}>
          <Text style={s.titulo}>Produtos</Text>
          <TouchableOpacity style={s.boxTotalCarrinho} onPress={goToCard}>
            <Text style={s.textTotalCarrinho}>{totalItemsCarrinho()}</Text>
          </TouchableOpacity>
        </View>

        <ScrollView
          ref={scrollRef}
          snapToInterval={width}
          snapToAlignment={'start'}
          decelerationRate={'fast'}
          horizontal
          showsHorizontalScrollIndicator={false}
          style={s.scrollView}>
          {products?.map((item) => <ProductItem {...item} onPress={updateCart} />)}
        </ScrollView>
      </View>
    </SafeAreaView>
  )
}