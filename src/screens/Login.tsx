import React, { useContext, useEffect, useState } from 'react'
import { StyleSheet, View, SafeAreaView, KeyboardAvoidingView, Platform, Text } from 'react-native'
import { InputTextComponent, Logo, Button, Loading, LabelText, Spacer } from '../components/index'
import { useNavigation, useRoute } from '@react-navigation/native'
import { doLogin } from '../service/api'
import { DataContext } from '../service/provider'

const s = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold'
  }
})

export default function LoginScreen() {

  const { setUser }: any = useContext(DataContext)

  const [loading, setLoading] = useState(false)
  const [error, setError] = useState(false)
  const [succesRegister, setSuccesRegister] = useState(false)
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [showPassword, setShowPassword] = useState(false)
  const navigation = useNavigation()
  const route = useRoute()

  const callBackLogin = async () => {
    setSuccesRegister(false)
    setError(false)
    setLoading(true)
    doLogin({ email, password })
      .then((res) => {
        console.log(res)
        setUser({ email, password })
        setError(false)
        navigation.replace('Home')
      })
      .catch((e) => {
        console.log(e)
        setError(true)
      })
      .finally(() => {
        setLoading(false)
      })
  }

  const goToRegister = () => navigation.navigate('Register')


  useEffect(() => {
    if (!!route.params?.registerSuccess) {
      setSuccesRegister(route.params?.registerSuccess)
    } else {
      setSuccesRegister(false)
    }
  }, [route.params?.registerSuccess])

  const disabledEntrar = email?.length == 0 || password?.length == 0



  return (
    <>
      <SafeAreaView style={{ flex: 1 }}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
          style={s.container}>
          <View style={[s.container, { width: '100%' }]}>
            <Logo />
            <Spacer space={5} />
            <InputTextComponent placeholder='Email' onChangeText={v => setEmail(v)} autoCapitalize='none' />
            <Spacer space={3} />
            <InputTextComponent placeholder='Password' secureTextEntry onChangeText={v => setPassword(v)} />
            <Spacer />
            {error && <LabelText text='Error inesperado, tente novamente' type='error' />}
            {succesRegister && <LabelText text='Cadastrado com sucesso' type='success' />}
            <Spacer space={5} />
            <Button text='Entrar' onPress={callBackLogin} disabled={disabledEntrar} />
            <Spacer space={2} />
            <Button text='Cadastrar-se' onPress={goToRegister} type='second' />
          </View>
          <Loading visible={loading} />
        </KeyboardAvoidingView>
      </SafeAreaView>
    </>
  )
}