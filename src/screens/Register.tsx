import React, { useState } from 'react'

import { View, SafeAreaView, KeyboardAvoidingView, Platform, StyleSheet, Text } from 'react-native'
import { Spacer, InputTextComponent, Loading, Button, Logo, LabelText } from '../components'
import { useNavigation } from '@react-navigation/native'
import { registerUser } from '../service/api'

const s = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold'
  }
})


export default function RegisterScreen() {
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState(false)

  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const navigation = useNavigation()

  const doRegister = () => {
    setLoading(true)
    registerUser({ name, email, password })
      .then((res) => {
        return navigation.navigate('Login', { registerSuccess: true })
      })
      .catch((e) => {
        setError(true)
      })
      .finally(() => {
        setLoading(false)
      })
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
        style={s.container}>
        <View style={[s.container, { width: '100%' }]}>
          <Logo />
          <Spacer space={5} />
          <InputTextComponent placeholder='Name' onChangeText={v => setName(v)} />
          <Spacer space={3} />
          <InputTextComponent placeholder='Email' keyboardType='email-address' onChangeText={v => setEmail(v)} autoCapitalize='none' />
          <Spacer space={3} />
          <InputTextComponent placeholder='Password' secureTextEntry onChangeText={v => setPassword(v)} />
          <Spacer />
          {error && <LabelText text='Error inesperado, tente novamente' type='error' />}
          <Spacer space={5} />
          <Button text='Cadastrar' onPress={doRegister} />
          <Spacer space={2} />
          <Button text='Voltar' onPress={() => navigation.navigate('Login', { registerSuccess: false })} type='second' />
        </View>
        <Loading visible={loading} />
      </KeyboardAvoidingView>
    </SafeAreaView>
  )
}