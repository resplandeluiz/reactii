import React, { useState } from 'react'
import { IProduct } from '../../service/model'
import { View, Image, Text, StyleSheet, TouchableOpacity } from 'react-native'
import { Spacer, Button } from '../../components'
import { useSafeAreaFrame } from 'react-native-safe-area-context'
import Icon from 'react-native-vector-icons/FontAwesome5';
import { useNavigation } from '@react-navigation/native'

const s = StyleSheet.create({
  box: {
    alignContent: 'center', alignItems: 'center', shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    height: 400,
    padding: 10,
  },
  img: {
    width: 300,
    height: 200,
  },
  boxInterno: {
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 10
  },
  center: {
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center'
  },
  boxTotal: {
    flexDirection: 'row', justifyContent: 'space-around',
  },
  title: {
    textAlign: 'center', fontSize: 20, fontWeight: 'bold'
  }
})

const ProductItem = (product: IProduct) => {

  const { width } = useSafeAreaFrame();
  const [total, setTotal] = useState(0)

  const navigation = useNavigation()
  const increment = () => setTotal(v => v + 1)
  const decrement = () => setTotal(v => v == 0 ? 0 : v - 1)

  const goToDetail = () => navigation.navigate('Detail', { product })

  return (
    <View style={s.center}>
      <View
        key={product._id + product.name}
        style={[s.box, { width: width }]}>
        <TouchableOpacity style={s.boxInterno} onPress={goToDetail}>
          <Image source={{ uri: product.imageUrl }} style={s.img} />
          <Spacer space={5} />
          <View style={s.center}>
            <Text style={s.title}>{product.name}</Text>
            <Spacer />

            <Text style={{ fontSize: 18 }}>R$ {product.price}<Text style={{ fontSize: 12 }}> /{product.unit}</Text></Text>
            <Spacer />

          </View>
          <View>

          </View>
        </TouchableOpacity>

      </View>
      <View style={s.center}>
        <View style={[s.boxTotal, { width: width }]}>
          <Icon
            size={20}
            name="minus"
            color='#3b5998'
            onPress={decrement}
          />
          <Text>{total}</Text>
          <Icon
            size={20}
            name="plus"
            color='#3b5998'
            onPress={increment}
          />
        </View>
        <Spacer space={5} />
        <Button text='Adicionar ao carrinho' disabled={total <= 0} onPress={() => {
          product.onPress({ _id: product._id, total })
          setTotal(0)
        }} />
      </View>
    </View>
  )
}

export default ProductItem