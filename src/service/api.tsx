import axios from 'axios';
import { ILoginUser, IRegisterUser } from './model';

const baseUrl = 'https://master--relaxed-fenglisu-464c83.netlify.app/.netlify/functions';

const registerUser = async (body: IRegisterUser) => {
  const url = `${baseUrl}/create-user`;
  const response = await axios.post(url, body);
  return response
};

const doLogin = async (body: ILoginUser) => {
  const url = `${baseUrl}/login-user`;
  const response = await axios.post(url, body);
  return response
}

const getProducts = async () => {
  const url = `${baseUrl}/read-products`;
  const response = await axios.get(url);
  return response
}

const createOrder = async (body) => {
  const url = `${baseUrl}/create-order`;
  const response = await axios.post(url, body);
  return response
}

export {
  registerUser,
  doLogin,
  getProducts,
  createOrder
}