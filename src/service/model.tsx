interface IRegisterUser {
  name: string
  email: string
  password: string
}

interface ILoginUser {
  email: string
  password: string
}
interface IProduct {
  _id: string
  category: ICategoryProduct
  description: string
  imageUrl: string
  name: string
  price: string
  unit: string
}

interface ICategoryProduct {
  id: string
  name: string
}

export {
  IRegisterUser,
  ILoginUser,
  IProduct
}