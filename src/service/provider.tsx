import React, { useState, createContext } from 'react'

export const DataContext = createContext({})

export const Provider = ({ children }: any) => {

  const [user, setUser] = useState(null)
  const [cart, setCart] = useState([])
  const [products, setProductsState] = useState(null)
  return (
    <DataContext.Provider
      value={{
        user,
        setUser,
        cart,
        setCart,
        products,
        setProductsState
      }}
      children={children} />)
}
